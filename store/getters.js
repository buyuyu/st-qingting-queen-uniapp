export default {
	windowHeight: (state) => {
		return state.systemInfo ? state.systemInfo.windowHeight : 0
	},
	windowWidth: (state) => {
		return state.systemInfo ? state.systemInfo.windowWidth : 0
	},
	statusBarHeight: (state) => {
		return state.systemInfo ? state.systemInfo.statusBarHeight : 0
	},
	platform: (state) => {
		return state.systemInfo ? state.systemInfo.platform : 'android'
	},
	screenWidth: (state) => {
		return state.systemInfo ? state.systemInfo.screenWidth : 0
	},
	device_id: (state) => {
		return state.systemInfo ? state.systemInfo.device_id : null
	},
	isLogin: () => {
		let token = null
		try {
		    token = uni.getStorageSync('TOKEN')
		} catch (e) {
		    // error
		}
		return !!token
	},
	username: (state) => {
		return state.userinfo.nickname || state.userinfo.username
	}
}