export const getClientId = () => {
	//获取客户端ID和版本号
	var clientid = '';
	// #ifdef APP-PLUS
	// 苹果系统
	plus.device.getInfo({
		success: function(e) {
			clientid = e.uuid;
			uni.setStorageSync('clientid', clientid);
		},
		fail: function(e) {
			console.log(e);
		}
	});
	// 安卓系统
	plus.device.getAAID({
		success: function(e) {
			clientid = e.aaid;
			console.log(clientid);
			uni.setStorageSync('clientid', clientid);
		},
		fail: function(e) {
			console.log(e);
		}
	});
	//老版本、安卓模拟器
	if (clientid == '') {
		clientid = plus.device.uuid;
		uni.setStorageSync('clientid', clientid);
	}
	// #endif
	console.log(clientid)
	return clientid;
}